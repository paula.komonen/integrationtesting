const pad = (hex) => {
    return (hex.length === 1 ? "0" + hex : hex );
}

module.exports = {
    rgbToHex: (red, green, blue) => {
        const redHex = red.toString(16);    // "02"
        const greenHex = green.toString(16);
        const blueHex = blue.toString(16);

        return pad(redHex) + pad(greenHex) + pad(blueHex); // ff0000
    },
     hexToRGB: (hex) => {
        const red = parseInt(hex.substring(0, 2), 16);
        const green = parseInt(hex.substring(0, 2), 16);
        const blue = parseInt(hex.substring(0, 2), 16);

        console.log(red, green, blue);
        return [red, green, blue];
}

}